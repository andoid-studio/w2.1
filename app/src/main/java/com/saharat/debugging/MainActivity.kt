package com.saharat.debugging

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
    }
    fun logging() {
        Log.v(TAG, "Hello, world!")
    }
    companion object {
        private const val TAG = "MainActivity"
    }
}